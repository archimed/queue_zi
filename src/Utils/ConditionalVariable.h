#ifndef CONDITIONALVARIABLE_H
#define	CONDITIONALVARIABLE_H

#include <pthread.h>

#include "LockGuard.h"
#include "NonCopyable.h"

namespace crutches
{

class condition_variable : non_copyable
{
public:
    condition_variable();
    virtual ~condition_variable();

    void notify();
    void notify_all();
    void wait(lock_guard& lock);

private:
    pthread_cond_t m_Conditional;
};

} // namespace crutches

#endif	/* CONDITIONALVARIABLE_H */

