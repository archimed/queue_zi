#ifndef THREADEXECUTOR_H
#define	THREADEXECUTOR_H

#include "Utils/NonCopyable.h"

namespace runnable
{

class MachineManager;

class ThreadExecutor : crutches::non_copyable
{
public:
    ThreadExecutor(MachineManager* pMgr);

public:
    void operator()();
    void Stop();

private:
    MachineManager* m_pMgr;
    bool m_lStop;
};

} // namespace runnable

#endif // THREADEXECUTOR_H
