#ifndef IEXECUTABLEMACHINE_H
#define	IEXECUTABLEMACHINE_H

#include "IMachineManager.h"

namespace runnable
{

class IExecutableMachine
{
public:

    IExecutableMachine(IMachineManager* pMgr) :
    m_pMgr(pMgr) { };

public:
    virtual void Proceed() = 0;

protected:

    void Schedule()
    {
        if (m_pMgr)
            m_pMgr->Schedule(this);
    }

private:
    IMachineManager* m_pMgr;
};

} // namespace runnable

#endif // IEXECUTABLEMACHINE_H
