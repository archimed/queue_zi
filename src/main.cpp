#include <iostream>
#include <string>

#include <unistd.h>

#include "Utils/Thread.h"
#include "Runnable/MachineManager.h"
#include "Runnable/ThreadExecutor.h"
#include "MessageQueue.h"
#include "StringMessageQueueWriter.h"
#include "StringMessageQueueReader.h"

int main()
{
    typedef MessageQueue<std::string> StringMessageQueue;
    StringMessageQueue queue(10000, 1000, 9000);

    long int nNumberOfCores = sysconf(_SC_NPROCESSORS_ONLN);
    if (nNumberOfCores < 1)
        nNumberOfCores = 1;
    std::cout << "Number of cores: " << nNumberOfCores << std::endl;

    std::vector<runnable::ThreadExecutor*> Executors;
    std::vector<crutches::thread*> Threads;

    runnable::MachineManager mgr;

    for (unsigned int i = 0; i < nNumberOfCores * 2; i++)
    {
        runnable::ThreadExecutor *E = new runnable::ThreadExecutor(&mgr);
        Executors.push_back(E);
        Threads.push_back(new crutches::thread(*E));
    }

    queue.put("test message", 10);
    queue.put("test message 2", 2);

    StringMessageQueueWriter writer1(&mgr, queue, "1", 4);
    StringMessageQueueWriter writer2(&mgr, queue, "2", 3);
    StringMessageQueueWriter writer31(&mgr, queue, "^", 2);
    StringMessageQueueWriter writer32(&mgr, queue, "v", 2);
    StringMessageQueueWriter writer4(&mgr, queue, "4", 1);
    StringMessageQueueReader reader(&mgr, queue);

    queue.set_events(&writer1);
    queue.set_events(&writer2);
    queue.set_events(&writer31);
    queue.set_events(&writer32);
    queue.set_events(&writer4);
    queue.set_events(&reader);

    queue.run();


    std::cout << "Waiting for 10 sec" << std::endl;
    usleep(10000000);

    queue.stop();

    for (std::vector<runnable::ThreadExecutor*>::iterator i = Executors.begin(); i != Executors.end(); ++i)
        (*i)->Stop();
    for (std::vector<crutches::thread*>::iterator i = Threads.begin(); i != Threads.end(); ++i)
        delete (*i);
    for (std::vector<runnable::ThreadExecutor*>::iterator i = Executors.begin(); i != Executors.end(); ++i)
        delete (*i);
    

    return 0;
}

