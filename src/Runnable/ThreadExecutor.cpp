#include "MachineManager.h"
#include "IExecutableMachine.h"
#include "ThreadExecutor.h"

namespace runnable
{

//==============================================================================

ThreadExecutor::ThreadExecutor(MachineManager* pMgr) :
m_pMgr(pMgr),
m_lStop(false) { }

//==============================================================================

void ThreadExecutor::operator()()
{
    while (!m_lStop)
    {
        crutches::mutex &Mutex = m_pMgr->getMutex();
        crutches::condition_variable &CV = m_pMgr->getConditionalVariable();
        IExecutableMachine *Machine = 0;

        {
            crutches::lock_guard lock(Mutex);
            MachineManager::t_Machine_queue &Machines = m_pMgr->getMachines();

            while (Machines.empty() && !m_lStop)
            {
                CV.wait(lock);
            }

            if (m_lStop)
                return;

            if (Machines.empty())
                continue;

            Machine = Machines.front();
            Machines.pop_front();
        }

        if (Machine)
            Machine->Proceed();
    }
}

//==============================================================================

void ThreadExecutor::Stop()
{
    crutches::mutex &Mutex = m_pMgr->getMutex();
    crutches::lock_guard lock(Mutex);
    m_lStop = true;
    m_pMgr->getConditionalVariable().notify_all();
}

//==============================================================================

} // namespace runnable
