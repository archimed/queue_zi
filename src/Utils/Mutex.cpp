#include <exception>

#include "Mutex.h"

namespace crutches
{

//==============================================================================

mutex::mutex()
{
    if (pthread_mutex_init(&m_Mutex, NULL) != 0) // init with default parameters
        throw std::exception(); // TODO: make more detailed exception
}

//==============================================================================

mutex::~mutex()
{
    pthread_mutex_destroy(&m_Mutex);
}

//==============================================================================

bool mutex::lock()
{
    return (pthread_mutex_lock(&m_Mutex) == PTHREAD_MUTEX_NORMAL);
}

//==============================================================================

bool mutex::unlock()
{
    return (pthread_mutex_unlock(&m_Mutex) == PTHREAD_MUTEX_NORMAL);
}

//==============================================================================

} // namespace crutches
