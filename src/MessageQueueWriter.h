#ifndef MESSAGEQUEUEWRITER_H
#define	MESSAGEQUEUEWRITER_H

#include <iostream>

#include "Writer.h"
#include "Runnable/IExecutableMachine.h"
#include "MessageQueue.h"

//==============================================================================

template<class MessageType>
class MessageQueueWriter : public Writer, public runnable::IExecutableMachine, public IMessageQueueEvents
{
public:
    MessageQueueWriter(runnable::IMachineManager* pMgr, MessageQueue<MessageType> &Queue);

    virtual ~MessageQueueWriter() { };

    virtual void run();

public:
    virtual void Proceed();

public:

    virtual void on_start()
    {
        Schedule();
    };

    virtual void on_stop()
    {
        stop();
    };

    virtual void on_hwm() { };

    virtual void on_lwm()
    {
        if (!m_lStop) Schedule();
    };

protected:
    virtual MessageType generate() = 0;
    virtual int get_priority() = 0;


    void stop();
    bool is_stopped() const;

private:
    MessageQueue<MessageType> &m_Queue;
    bool m_lStop; // it is not necessary to be atomic
};


//==============================================================================

template<class MessageType>
MessageQueueWriter<MessageType>::MessageQueueWriter(runnable::IMachineManager* pMgr,
                                                    MessageQueue<MessageType> &Queue) :
IExecutableMachine(pMgr),
m_Queue(Queue),
m_lStop(false) { }

//==============================================================================

template<class MessageType>
void MessageQueueWriter<MessageType>::run()
{
    if (m_lStop)
        return;

    RetCodes::type result = m_Queue.put(generate(), get_priority());

    switch (result)
    {
        case RetCodes::NO_SPACE:
            std::cout << std::endl << "NO_SPACE" << std::endl;
            // Do not schedule machine for execution
            return;
        case RetCodes::HWM:
            std::cout << std::endl << "HWM" << std::endl;
            // Do not schedule machine for execution
            return;
        case RetCodes::STOPED:
            std::cout << std::endl << "STOPPED" << std::endl;
            m_lStop = true;
            // Do not schedule machine for execution
            return;
        default:
            Schedule();
            return;
    }
}

//==============================================================================

template<class MessageType>
bool MessageQueueWriter<MessageType>::is_stopped() const
{
    return m_lStop;
}

//==============================================================================

template<class MessageType>
void MessageQueueWriter<MessageType>::Proceed()
{
    run();
}

//==============================================================================

template<class MessageType>
void MessageQueueWriter<MessageType>::stop()
{
    m_lStop = true;
}

//==============================================================================

#endif // MESSAGEQUEUEWRITER_H
