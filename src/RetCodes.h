#ifndef RETCODES_H
#define	RETCODES_H

struct RetCodes
{
    typedef const int tCode;
    typedef int type;

    static tCode OK = 0;
    static tCode HWM = -1;
    static tCode NO_SPACE = -2;
    static tCode STOPED = -3;
};

#endif // RETCODES_H
