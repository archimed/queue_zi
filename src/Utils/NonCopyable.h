#ifndef NONCOPYABLE_H
#define	NONCOPYABLE_H

namespace crutches
{

class non_copyable // like boost::non_copyable
{
public:

    non_copyable() { }
    virtual ~non_copyable() { }
private:
    non_copyable(const non_copyable&);
    non_copyable& operator=(const non_copyable&);
};

} // namespace crutches


#endif	/* NONCOPYABLE_H */

