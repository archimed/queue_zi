#ifndef STRINGMESSAGEQUEUEREADER_H
#define	STRINGMESSAGEQUEUEREADER_H

#include "MessageQueueReader.h"

//==============================================================================

namespace
{
typedef std::string MessageType;
}

class StringMessageQueueReader : public MessageQueueReader<MessageType>
{
public:
    StringMessageQueueReader(runnable::IMachineManager* pMgr, MessageQueue<MessageType> &Queue);

    virtual ~StringMessageQueueReader() { };
    
    virtual void handle_message(const MessageType &Message);
};

//==============================================================================

#endif	/* STRINGMESSAGEQUEUEREADER_H */

