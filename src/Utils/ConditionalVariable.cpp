#include <exception>

#include "ConditionalVariable.h"

namespace crutches
{

//==============================================================================

condition_variable::condition_variable()
{
    if (pthread_cond_init(&m_Conditional, NULL) != 0)
        throw std::exception();
}

//==============================================================================

condition_variable::~condition_variable()
{
    pthread_cond_destroy(&m_Conditional);
}

//==============================================================================

void condition_variable::wait(lock_guard& lock)
{
    pthread_cond_wait(&m_Conditional, &lock.m_Mutex.m_Mutex); // TODO: check return code
}

//==============================================================================

void condition_variable::notify()
{
    pthread_cond_signal(&m_Conditional);
}

//==============================================================================

void condition_variable::notify_all()
{
    pthread_cond_broadcast(&m_Conditional);
}

//==============================================================================

} // namespace crutches

