#ifndef READER_H
#define	READER_H

template<class MessageType>
class Reader
{
public:
    virtual void run() = 0;
    virtual void handle_message(const MessageType &Message) = 0;

    virtual ~Reader() { };
};

#endif // READER_H
