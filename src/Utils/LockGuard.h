#ifndef LOCKGUARD_H
#define	LOCKGUARD_H

#include "NonCopyable.h"
#include "Mutex.h"

namespace crutches
{

typedef crutches::mutex t_Mutex;

class lock_guard : non_copyable // something like boost::lock_guard
{
public:
    friend class condition_variable;

    lock_guard(t_Mutex &Mutex) throw ();
    virtual ~lock_guard() throw ();

public:
    void lock() throw ();
    void unlock() throw ();

private:
    t_Mutex& m_Mutex;
};

} // namespace crutches

#endif	/* LOCKGUARD_H */

