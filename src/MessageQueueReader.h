#ifndef MESSAGEQUEUEREADER_H
#define	MESSAGEQUEUEREADER_H

#include "Reader.h"
#include "Runnable/IExecutableMachine.h"
#include "MessageQueue.h"

//==============================================================================

template<class MessageType>
class MessageQueueReader : public Reader<MessageType>, public runnable::IExecutableMachine, public IMessageQueueEvents
{
public:
    MessageQueueReader(runnable::IMachineManager* pMgr, MessageQueue<MessageType> &Queue);

    virtual ~MessageQueueReader() { };

    virtual void run();

public:
    virtual void Proceed();

public:

    virtual void on_start()
    {
        Schedule();
    };

    virtual void on_stop()
    {
        stop();
    };

    virtual void on_hwm() { };

    virtual void on_lwm() { };

protected:
    bool is_stopped() const;
    void stop();

private:
    MessageQueue<MessageType> &m_Queue;
    bool m_lStop; // it is not necessary to be atomic
};

//==============================================================================

template<class MessageType>
MessageQueueReader<MessageType>::MessageQueueReader(runnable::IMachineManager* pMgr,
                                                    MessageQueue<MessageType> &Queue) :
IExecutableMachine(pMgr),
m_Queue(Queue),
m_lStop(false) { }

//==============================================================================

template<class MessageType>
void MessageQueueReader<MessageType>::run()
{
    MessageType msg;
    RetCodes::type nRetCode = m_Queue.get(msg);

    switch (nRetCode)
    {
        case RetCodes::STOPED:
            m_lStop = true;
            return;
        default:
            this->handle_message(msg);
            if (!m_lStop)
                Schedule();
            return;
    }
}

//==============================================================================

template<class MessageType>
bool MessageQueueReader<MessageType>::is_stopped() const
{
    return m_lStop;
}

//==============================================================================

template<class MessageType>
void MessageQueueReader<MessageType>::Proceed()
{
    run();
}

//==============================================================================

template<class MessageType>
void MessageQueueReader<MessageType>::stop()
{
    m_lStop = true;
}

//==============================================================================

#endif	/* MESSAGEQUEUEREADER_H */

