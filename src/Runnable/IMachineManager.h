#ifndef IMACHINEMANAGER_H
#define	IMACHINEMANAGER_H

#include <functional>

namespace runnable
{

class IExecutableMachine;

class IMachineManager
{
public:

    virtual ~IMachineManager() { };
    virtual void Schedule(IExecutableMachine *pMachine) = 0;
    virtual void Proceed() = 0;
};

} // namespace runnable


#endif // #ifndef IMACHINEMANAGER_H
