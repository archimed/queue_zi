#include "StringMessageQueueWriter.h"

//==============================================================================

StringMessageQueueWriter::StringMessageQueueWriter(runnable::IMachineManager* pMgr,
                                                   MessageQueue<MessageType> &Queue,
                                                   const char *sName,
                                                   int nPriority) :
MessageQueueWriter<std::string>(pMgr, Queue),
m_sName(sName),
m_nPriority(nPriority) { }

//==============================================================================

MessageType StringMessageQueueWriter::generate()
{
    return m_sName;
}

//==============================================================================

int StringMessageQueueWriter::get_priority()
{
    return m_nPriority;
}

//==============================================================================

