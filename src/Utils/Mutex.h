#ifndef MUTEX_H
#define	MUTEX_H

#include <pthread.h>

#include "NonCopyable.h"

namespace crutches
{

class mutex : non_copyable
{
public:
    friend class condition_variable;

    mutex();
    virtual ~mutex();

public:
    bool lock();
    bool unlock();

private:
    pthread_mutex_t m_Mutex;
};

} // namespace crutches

#endif	/* MUTEX_H */

