#include <iostream>

#include "StringMessageQueueReader.h"

StringMessageQueueReader::StringMessageQueueReader(runnable::IMachineManager* pMgr, MessageQueue<MessageType> &Queue) :
MessageQueueReader<MessageType> (pMgr, Queue) { }

void StringMessageQueueReader::handle_message(const MessageType &Message)
{
    std::cout << Message;
}
