#include "LockGuard.h"

namespace crutches
{

//==============================================================================

lock_guard::lock_guard(t_Mutex& Mutex) throw () :
m_Mutex(Mutex)
{
    m_Mutex.lock();
}

//==============================================================================

lock_guard::~lock_guard() throw ()
{
    m_Mutex.unlock();
}

//==============================================================================

void lock_guard::lock() throw ()
{
    m_Mutex.lock();
}

//==============================================================================

void lock_guard::unlock() throw ()
{
    m_Mutex.unlock();
}

//==============================================================================

} // namespace crutches
