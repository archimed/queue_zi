#ifndef MACHINEMANAGER_H
#define	MACHINEMANAGER_H

#include <list>
#include <memory>

#include "Utils/Mutex.h"
#include "Utils/ConditionalVariable.h"

#include "IMachineManager.h"

namespace runnable
{

class IMachineManager;
class IExecutableMachine;

class MachineManager : public IMachineManager
{
    typedef std::list<IExecutableMachine *> t_Machine_queue;
    friend class ThreadExecutor;

public:
    MachineManager() { };
    virtual ~MachineManager() { };


public:
    virtual void Proceed();
    virtual void Schedule(IExecutableMachine *pMachine);

protected:
    t_Machine_queue& getMachines();
    crutches::condition_variable& getConditionalVariable();
    crutches::mutex& getMutex();

private:
    t_Machine_queue m_Machines;
    crutches::condition_variable m_ConditionalVariable;
    crutches::mutex m_Mutex;
};

} // namespace runnable

#endif // MACHINEMANAGER_H
