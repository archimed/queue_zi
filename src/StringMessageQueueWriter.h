/* 
 * File:   StringMessageQueueWriter.h
 * Author: morozov
 *
 * Created on February 12, 2015, 2:56 PM
 */

#ifndef STRINGMESSAGEQUEUEWRITER_H
#define	STRINGMESSAGEQUEUEWRITER_H

#include <string>
#include <memory>

#include "MessageQueueWriter.h"

namespace
{
typedef std::string MessageType;
}

class StringMessageQueueWriter : public MessageQueueWriter<MessageType>
{
public:
    StringMessageQueueWriter(runnable::IMachineManager* pMgr,
                             MessageQueue<MessageType> &Queue,
                             const char *sName,
                             int nPriority = 0);

    virtual ~StringMessageQueueWriter() { };

protected:
    virtual MessageType generate();
    virtual int get_priority();

private:
    const std::string m_sName;
    const int m_nPriority;
};

#endif	/* STRINGMESSAGEQUEUEWRITER_H */

