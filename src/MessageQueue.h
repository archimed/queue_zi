#pragma once

#include <queue>
#include <vector>

#include "Utils/Mutex.h"
#include "Utils/ConditionalVariable.h"

#include "RetCodes.h"
#include "IMessageQueueEvents.h"

//==============================================================================

template<class MessageType>
class MessageQueue
{
public:
    MessageQueue(std::size_t queue_size, std::size_t lwm, std::size_t hwm); // I'd prefer std::size_t

    virtual ~MessageQueue() { };

public:
    RetCodes::type put(const MessageType &message, int priority);
    RetCodes::type get(MessageType &message);
    void stop();
    void run();
    void set_events(IMessageQueueEvents *events);

protected:

    struct QueueElement
    {

        QueueElement(int nPriority, const MessageType &Element) :
        m_nPriority(nPriority), m_Element(Element) { }

        bool operator<(const QueueElement &other) const
        {
            return m_nPriority < other.m_nPriority;
        }

        int m_nPriority;
        MessageType m_Element;
    };

    template <class T>
    class reservable_priority_queue : public std::priority_queue<T>
    {
    public:
        typedef typename std::priority_queue<T>::size_type size_type;

        reservable_priority_queue(size_type capacity = 0)
        {
            this->c.reserve(capacity); // a hack to access the underlying container
        };
    };

private:
    const std::size_t m_nMaxQueueSize;
    const std::size_t m_nLWM;
    const std::size_t m_nHWM;

    typedef std::vector<IMessageQueueEvents*> tEventSubscribers;
    tEventSubscribers m_Events;

    reservable_priority_queue<QueueElement> m_Queue;
    crutches::mutex m_Mutex;
    crutches::condition_variable m_ConditionalVariable;
    bool m_lStop; // it is not necessary to be atomic
};


//==============================================================================

template<class MessageType>
MessageQueue<MessageType>::MessageQueue(std::size_t queue_size, std::size_t lwm, std::size_t hwm) :
m_nMaxQueueSize(queue_size),
m_nLWM(lwm),
m_nHWM(hwm),
m_Queue(queue_size),
m_lStop(false) { }

//==============================================================================

template<class MessageType>
RetCodes::type MessageQueue<MessageType>::put(const MessageType &message, int priority)
{
    std::size_t nSize = 0;
    bool lHWMEvent = false;

    {
        crutches::lock_guard lock(m_Mutex);
        nSize = m_Queue.size();

        if (nSize >= m_nMaxQueueSize)
            return RetCodes::NO_SPACE;

        m_Queue.push(QueueElement(priority, message));

        if (nSize == m_nHWM)
            lHWMEvent = true;
    }

    m_ConditionalVariable.notify_all();

    if (lHWMEvent)
    {
        for (tEventSubscribers::iterator i = m_Events.begin(); i != m_Events.end(); ++i)
            (*i)->on_hwm();
    }
    return (nSize >= m_nHWM ? RetCodes::HWM : RetCodes::OK);
}

//==============================================================================

template<class MessageType>
RetCodes::type MessageQueue<MessageType>::get(MessageType &message)
{
    bool lLWMEvent = false;

    {
        crutches::lock_guard lock(m_Mutex);

        while (m_Queue.empty() && !m_lStop)
            m_ConditionalVariable.wait(lock);

        if (m_lStop)
        {
            return RetCodes::STOPED;
        }

        message = m_Queue.top().m_Element;
        m_Queue.pop();

        if (m_Queue.size() == m_nLWM)
            lLWMEvent = true;
    }

    if (lLWMEvent)
    {
        for (tEventSubscribers::iterator i = m_Events.begin(); i != m_Events.end(); ++i)
            (*i)->on_lwm();
    }

    return RetCodes::OK;
}

//==============================================================================

template<class MessageType>
void MessageQueue<MessageType>::stop()
{
    m_lStop = true;
    for (tEventSubscribers::iterator i = m_Events.begin(); i != m_Events.end(); ++i)
        (*i)->on_stop();
    m_ConditionalVariable.notify_all();
}

//==============================================================================

template<class MessageType>
void MessageQueue<MessageType>::run()
{
    for (tEventSubscribers::iterator i = m_Events.begin(); i != m_Events.end(); ++i)
        (*i)->on_start();
}

//==============================================================================

template<class MessageType>
void MessageQueue<MessageType>::set_events(IMessageQueueEvents *events)
{
    if (events)
        m_Events.push_back(events);
}

//==============================================================================
