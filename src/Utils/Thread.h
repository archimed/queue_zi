#ifndef THREAD_H
#define	THREAD_H

#include <exception>
#include <pthread.h>

#include "NonCopyable.h"

namespace crutches
{

class thread : non_copyable
{

    template<class Fun>
    class runner
    {
    public:

        static void *start(void *ptr)
        {
            Fun &function = *((Fun*) ptr);
            function();
            pthread_exit(NULL);
            return 0;
        }
    };

public:

    template<class F>
    thread(F& pRunner)
    {
        if (pthread_create(&m_Thread, NULL, &(runner<F>::start), (void*) &pRunner) != 0)
            throw std::exception();
    }

    virtual ~thread()
    {
//        ask_stop();
        join();
    }

    bool ask_stop()
    {
        return pthread_cancel(m_Thread) == 0;
    }

    bool join()
    {
        return pthread_join(m_Thread, NULL) == 0;
    }

private:
    pthread_t m_Thread;
};

} // namespace crutches

#endif	/* THREAD_H */

