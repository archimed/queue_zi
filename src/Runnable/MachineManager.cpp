#include "Utils/LockGuard.h"
#include "IExecutableMachine.h"
#include "MachineManager.h"

namespace runnable
{

//==============================================================================

void MachineManager::Proceed()
{
    IExecutableMachine *pMachine = 0;
    {
        crutches::lock_guard lock(m_Mutex);
        if (m_Machines.empty())
            return;
        pMachine = m_Machines.front();
        m_Machines.pop_front();
    }

    if (pMachine)
        pMachine->Proceed();
}

//==============================================================================

void MachineManager::Schedule(IExecutableMachine* pMachine)
{
    if (!pMachine)
        return;

    {
        crutches::lock_guard lock(m_Mutex);
        m_Machines.push_back(pMachine);
        m_ConditionalVariable.notify_all();
    }
}

//==============================================================================

MachineManager::t_Machine_queue& MachineManager::getMachines()
{
    return m_Machines;
}

//==============================================================================

crutches::condition_variable& MachineManager::getConditionalVariable()
{
    return m_ConditionalVariable;
}

//==============================================================================

crutches::mutex& MachineManager::getMutex()
{
    return m_Mutex;
}

//==============================================================================

} // namespace runnable
